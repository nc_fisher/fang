module Main where

--import Paths_f_lang
import PlatformIndependence
import Parser
import Tokenizer
import Compiler
import AbstractMachine
import InterpreterUI
import System.IO (stdout, hSetBuffering, BufferMode(BlockBuffering)) --needed for repl
import Control.Exception (SomeException, try)
import System.Environment
--import System.IO

main :: IO()
main = do
    hSetBuffering stdout (BlockBuffering (Just 4096)) --needed for repl
    commandLnArgs <- getArgs
    putStr "\n"
    case commandLnArgs of
        ("repl":_)      -> interpreterUI
        [file,"debug"]  -> do
                            fProgram <- try (platformIndependentReadFile file) :: IO (Either SomeException String)
                            case fProgram of
                                Left exception -> print exception
                                Right program -> printAll program
        [file]          -> do
                            fProgram <- try (platformIndependentReadFile file) :: IO (Either SomeException String)
                            case fProgram of
                                Left exception -> print exception
                                Right program -> putStrLn $ fst (execute (compilerHandler (parseSynTree (tokenizeEither (tokenize program))))) ++ "\n"
        _               -> do 
                            putStrLn asciiArt
                            putStrLn "Call the Interpreter onto a file to compile and execute the F-Code or call the FANG - REPL.\n"