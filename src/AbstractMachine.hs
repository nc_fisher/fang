module AbstractMachine (
    Snapshot(..),
    History,
    address, add2arg, new, value, arity,
    reset,
    pushFun,
    pushVal,
    operator,
    halt,
    AbstractMachine.return,
    pushPre,
    update,
    pushParam,
    makeApp,
    slide,
    unwind,
    call,
    alloc,
    updateLet,
    slideLet,
    showHistory, showMachineSteps, showResult,
    execute,
    mainLoop
    ) where

import Compiler
import Data.Tuple
import Data.List
import Data.Either

-- in order to save the context of each command executed by the abstract machine
newtype Snapshot   = Snapshot (State, Instruction)
type    History    = [Snapshot]
type    NameLength = Int
type    RowWidth   = Int
type    RowLength  = Int
type    TableWidth = Int

type RuntimeEnv           = (History, Trace, [CodeCellDescription])
type Trace                = [String]
type CodeCellDescription  = (Int, String)

-- adjust to terminal size. Output consists of four rows -> terminal at least 100 characters wide
defaultRowWidth :: RowWidth
defaultRowWidth = 15

{- Helper functions:
    address
    add2arg
    new
    typ -- deprecated
    value
    arity
    evalOpUn
    evalOpBin
    -}

-- returns heap address of a function definition.
address :: FunctionName -> Global -> Either String  HeapAddress
address fName global = _address (lookup fName global) where
    _address Nothing         = Left $ "Compilation error: Function \"" ++ fName ++ "\" not defined."
    _address (Just hAddress) = Right hAddress

-- If the actual parameter HeapAdress references a HeapCell with structure
-- APP adr1 adr2, then adr2 two is returned.
add2arg :: HeapAddress -> Heap -> Either String HeapAddress
add2arg hadr heap = _add2 (heap !! hadr) where
    _add2 (APP _ adr2) = Right adr2
    _add2 (IND adr)    = add2arg adr heap
    _add2 hcell        = Left $ "Compilation error: HeapCell \"" ++ show hcell ++
                                "\" referenced by \"" ++ show hadr ++ "\" is no APP-Cell."

-- Adds a cell to the heap and returns its address together with the updated heap.
new :: HeapCell -> Heap -> (HeapAddress,Heap)
new hcell heap = let hnew = heap ++ [hcell]
                  in (length hnew - 1, hnew)

{- redundant, used pattern matching instead
typ :: HeapAddress -> Heap -> Either String PossibleTypes
typ hadr heap = _typ (heap !! hadr) where
    _typ (VAL (Truthval _)) = Right $ Truthval True
    _typ (VAL (Integer  _)) = Right $ Integer  1
    _typ hcell              = Left  $ "Compilation error: HeapCell \"" ++ show hcell ++
                                      "\" referenced by \"" ++ show hadr ++ "\" is no VAL-Cell."-}

-- Returns content of heap address unless the heap address has structure IND adr.
-- In that case the content of the heap cell referenced by adr is returned.
value :: HeapAddress -> Heap -> HeapCell
value hadr heap = case heap !! hadr of
                IND adr -> value adr heap
                _     -> heap !! hadr

-- Returns the arity of a predefined operator.
arity :: CalcOp -> Arity
arity OpNot   = 1
arity OpSubUn = 1
arity OpIf    = 3
arity _       = 2

-- Evaluation of Operators
-- Unary operators
evalOpUn :: CalcOp -> PossibleTypes -> Either String PossibleTypes
evalOpUn OpSubUn (Integer v)     = Right $ Integer  $ -v
evalOpUn OpSubUn _               = Left "Runtime error: Unary minus applied to boolean"
evalOpUn OpNot   (Truthval bool) = Right $ Truthval $ not bool
evalOpUn OpNot   _               = Left "Runtime error: Logical not applied to number"
evalOpUn _       _               = Left "Compilation error: No valid predefined unary operator"

-- Binary Operators
evalOpBin :: CalcOp -> PossibleTypes -> PossibleTypes -> Either String PossibleTypes
evalOpBin opInt    (Integer int1)   (Integer int2)   = arBin opInt int1 int2 where
    arBin OpEq    x y = Right $ Truthval $ x == y
    arBin OpLess  x y = Right $ Truthval $ x < y
    arBin OpTimes x y = Right $ Integer  $ x * y
    arBin OpDiv   _ 0 = Left "Runtime error: Division by zero"
    arBin OpDiv   x y = Right $ Integer  $ x `div` y
    arBin OpAdd   x y = Right $ Integer  $ x + y
    arBin OpSubBi x y = Right $ Integer  $ x - y
    arBin _       _ _ = Left "Runtime error: Logical operator applied to numbers"
evalOpBin opLog    (Truthval bool1) (Truthval bool2) = loBin opLog bool1 bool2 where
    loBin OpAnd a b = Right $ Truthval $ a && b
    loBin OpOr  a b = Right $ Truthval $ a || b
    loBin OpEq  a b = Right $ Truthval $ a == b
    loBin _     _ _ = Left "Runtime error: Arithmetic operator applied to booleans"
evalOpBin _     _ _ = Left "Runtime error: Binary operator applied to mixed types"

{- Machine instructions:
    reset
    pushFun
    pushVal
    operator
    halt
    return
    pushPre
    update
    pushParam
    makeApp
    slide
    unwind
    call
    alloc
    updateLet
    slideLet
    -}

reset :: Either String State -> Either String State
reset state = do
    State (_, code, (stack, _), heap, global) <- state
    let newPC = 0
        newTop = -1
        --Find InstructionRegister and update all relevant elements
    Prelude.return $ State (newPC, code, (stack, newTop), heap, global)

pushFun :: FunctionName -> Either String State -> Either String State
pushFun funName state = do
    State (pc, code, (stack, top), heap, global) <- state
    hadr <- address funName global
    Prelude.return $ State (pc, code, (stack ++ [HEAP hadr], top + 1), heap, global)

pushVal ::  HeapCell -> Either String State -> Either String State
pushVal (VAL val) state = do
    State (pc, code, (stack, top), heap, global) <- state
    let (newStackElem, newHeap) = new (VAL val) heap
    Prelude.return $ State (pc, code, (stack ++ [HEAP newStackElem], top + 1), newHeap, global)

pushVal _ _ = Left "Compilation error: attempting to push a heapcell that does not hold a value."

-- Evaluate predefined operators
-- Prepares stack for use of update:
--  - top cell references the operator evaluation's result
--  - (top - 1) cell contains the return address
--  - (top - 2) cell references application spine's root (APP-cell in heap)
operator :: OperatorKind -> Either String State -> Either String State
operator (OpInt 1) state = do
    State (pc, code, (stack, top), heap, global) <- state
    let HEAP hadr1       = stack !! (top - 2)
        PRE  unOp 1      = value hadr1 heap
        HEAP hadr2       = stack !! top
        VAL  param       = value hadr2 heap
        returnAdr        = stack !! (top - 1)
    result <- evalOpUn unOp param
    let newHeap = new (VAL result) heap
    -- remove topmost 3 stack elements and append 2 new elements
    Prelude.return $ State (pc, code, (take (top + 1 - 3) stack ++ [returnAdr, HEAP (fst newHeap)],
                            top - 1), snd newHeap, global)

operator (OpInt 2) state = do
    State (pc, code, (stack, top), heap, global) <- state
    let HEAP hadr1       = stack !! (top - 3)
        PRE  binOp 2     = value hadr1 heap
        HEAP hadr2       = stack !! (top - 1)
        VAL  param1      = value hadr2 heap
        HEAP hadr3       = stack !! top
        VAL  param2      = value hadr3 heap
        returnAdr        = stack !! (top - 2)
    result <- evalOpBin binOp param1 param2
    let newHeap = new (VAL result) heap
    -- remove topmost 5 stack elements and append 2 new elements
    Prelude.return $ State (pc, code, (take (top + 1 - 5) stack ++ [returnAdr, HEAP (fst newHeap)],
                            top - 3), snd newHeap, global)

operator If state = do
    State (pc, code, (stack, top), heap, global) <- state
    let HEAP hadr             = stack !! top
        CODE returnAdr        = stack !! (top - 1)
        HEAP thenAPPAdr       = stack !! (top - 4)
        HEAP elseAPPAdr       = stack !! (top - 5)
    thenAdr <- add2arg thenAPPAdr heap
    elseAdr <- add2arg elseAPPAdr heap
    case value hadr heap of
          VAL (Truthval bool) ->
              let branchAdr = if bool then thenAdr else elseAdr in
                Prelude.return $ State (pc, code, (take (top + 1 - 5) stack ++
                                              [CODE returnAdr, HEAP branchAdr], top - 3),
                                              heap, global)
          _  -> Left "Runtime error: if condition not boolean"

operator _ _ = Left "Compilation error: No valid predefined operator handed to function operator."

-- Halt the abstract machine, keep the current state and determine whether it's a valid end state.
-- Depends on mainLoop which then aborts execution.  Doesn't effect errors already handed over.
halt :: Either String State -> Either String State
halt state = do
       State (_, _, (stack, top), _, _) <- state
       case length stack of
            0 -> Left "Compilation error: Stack is empty, no result available"
            1 -> if top /= 0 then Left "Abstract Machine error: top inconsistent with stack"
                             else state
            _ -> Left $ "Compilation error: Stack holds more than one element, execution aborted." ++
                              "\n Stack content:\n" ++ show stack

-- Return to saved code address saved in the stack cell below the top. Overwrites return
-- address with the function application's result, e.g. the top.
return :: Either String State -> Either String State
return state = do
    State (_, code, (stack, top), heap, global) <- state
    let CODE pcNew = stack !! (top - 1)
        appResult  = stack !! top
    Prelude.return $ State (pcNew, code, (take (top + 1 - 2) stack ++ [appResult], top - 1),
                            heap, global)

-- Save predefined operation to heap and push pointer to stack.
pushPre :: CalcOp -> Either String State -> Either String State
pushPre calcOp state = do
    State (pc, code, (stack, top), heap, global) <- state
    let (hAdr, newHeap) = new (PRE calcOp (arity calcOp)) heap
    Prelude.return $ State (pc, code, (stack ++ [HEAP hAdr], top + 1), newHeap, global)

-- case op: save the result of a predefined function's application to the corresponding APP-cell.
--          appAdr references former APP-cell which then contains the copy of the application's result.
-- case n:  replace function definition by pointer to the application spine's leaf of it's
--          corresponding application tree that was previously built in the heap
update :: OperatorKind -> Either String State -> Either String State
update Op state  = do
    State (pc, code, (stack, top), heap, global) <- state
    let HEAP appAdr = stack !! (top - 2)
        CODE retAdr = stack !! (top - 1)
        HEAP valAdr = stack !! top
        newHeap     = take appAdr heap ++ heap !! valAdr : drop (appAdr + 1) heap
    Prelude.return $ State (pc, code, (take (top + 1 - 3) stack ++ [CODE retAdr, HEAP appAdr],
                            top - 1),
                            newHeap, global)

update (OpInt funArity) state = do
    State (pc, code, (stack, top), heap, global) <- state
    let HEAP defAdr       = stack !! (top - funArity - 2)
        HEAP appSpineLeaf = stack !! top
        newHeap     = take defAdr heap ++ IND appSpineLeaf : drop (defAdr + 1) heap
    Prelude.return $ State (pc, code, (stack, top), newHeap, global)

update If _ = Left "Compilation error: update cannot be used for \"if\""


pushParam :: Int -> Either String State -> Either String State
pushParam n state = do
    State (pc, code, (stack, top), heap, global) <- state
    let HEAP stackParam = stack !! (top - n - 1)
    newHadr <- add2arg stackParam heap
    Prelude.return $ State (pc, code, (stack ++ [HEAP newHadr], top + 1), heap, global)


makeApp :: Either String State -> Either String State
makeApp state = do
    State (pc, code, (stack, top), heap, global) <- state
    let HEAP newHadr1 = stack !! top
        HEAP newHadr2 = stack !! (top - 1)
        (replacedStackAddress, newHeap) = new (APP newHadr1 newHadr2) heap
        newStack = take (length stack - 2) stack ++ [HEAP replacedStackAddress]
    Prelude.return $ State (pc, code, (newStack, top - 1), newHeap, global)

slide :: Arity -> Either String State -> Either String State
slide arty state = do
    State (pc, code, (stack, top), heap, global) <- state
    let newStack = take (length stack - arty - 1 - 1) stack ++ drop (length stack -2) stack
    Prelude.return $ State (pc, code, (newStack, top - arty), heap, global)

unwind :: Either String State -> Either String State
unwind state = do
    State (pc, code, (stack, top), heap, global) <- state
    let HEAP topStackHadr = stack !! top
    case value topStackHadr heap of APP adr1 _ -> Prelude.return $ State (pc -1, code, (stack ++ [HEAP adr1], top +1), heap, global)
                                    _          -> Prelude.return $ State (pc, code, (stack, top), heap, global)


call :: Either String State -> Either String State
call state = do
    State (pc, code, (stack, top), heap, global) <- state
    let HEAP topStackHadr = stack !! top
    case value topStackHadr heap of
        VAL _        -> Prelude.return $ State (pc + 1, code, (stack, top), heap, global)
        DEF _ _ cAdr -> Prelude.return $ State (cAdr, code, (stack ++ [CODE pc], top + 1), heap, global)
        PRE _ 2      -> Prelude.return $ State (4, code, (stack ++ [CODE pc], top + 1), heap, global)
        PRE OpIf 3   -> Prelude.return $ State (13, code, (stack ++ [CODE pc], top + 1), heap, global)
        PRE _ 1      -> Prelude.return $ State (21, code, (stack ++ [CODE pc], top + 1), heap, global)
        PRE _ _      -> Left "Compilation error: There is no predefined function that has arity > 3."
        _            -> Left "Compilation error: Invalid input." -- The case of (IND _) would not be caught in this function, because of recursion for (IND _) in 'value' function

alloc :: Either String State -> Either String State
alloc state = do
    State (pc, code, (stack, top), heap, global) <- state
    let (newTopAdr, newHeap) = new UNINITIALIZED heap
    Prelude.return $ State (pc, code, (stack ++ [HEAP newTopAdr], top + 1), newHeap, global)


updateLet :: Int -> Either String State -> Either String State
updateLet n state = do
    State (pc, code, (stack, top), heap, global) <- state
    let HEAP hAdr   = stack !! (top - n - 1)
        HEAP topAdr = stack !! top
    arg <- add2arg hAdr heap
    let newHeap  = take arg heap ++ IND topAdr : drop (arg + 1) heap
        newStack = take (length stack - 1) stack
    Prelude.return $ State (pc, code, (newStack, top - 1), newHeap, global)


slideLet :: Int -> Either String State -> Either String State
slideLet n state = do
    State (pc, code, (stack, top), heap, global) <- state
    let stackTop = stack !! top
        newStack = take (length stack - n - 1) stack ++ [stackTop]
    Prelude.return $ State (pc, code, (newStack, top - n), heap, global)


{- Functions related to showHistory
    showHistory
    showSnapshot
    pointerRow
    stackRow
    addTopPointer
    heapRow
    globalRow
    widthTable
    maxRowWidthPointer
    maxRowWidthStack
    maxRowWidthHeap
    maxRowWidthGlobal
    maxHeapCellLength
    maxLengthGlobalContent
    maxLengthHeapContent
    maxNameLengthStack
    stackLongestAddress
    maxNameLengthHeap
    heapLongestAddress
    maxNameLengthCode
    showI
    showT
    showP
    showS
    padName
    padCell
    padRow
    rowNaming
    hline
   -}

-- not used, helpful for debugging
-- instance Show Snapshot where
-- show snapshot = showHistory [snapshot]

-- show history of executed instructions including states after their execution
showHistory :: RuntimeEnv -> String
showHistory ([],[],[]) = "No History to show."
showHistory (history, _, descriptions) =
    let rowWidthPointer = maxRowWidthPointer history
        rowWidthStack   = maxRowWidthStack   history descriptions
        rowWidthHeap    = maxRowWidthHeap    history
        rowWidthGlobal  = maxRowWidthGlobal  history
        nameLengthStack = maxNameLengthStack history
        nameLengthHeap  = maxNameLengthHeap  history
        nameLengthCode  = maxNameLengthCode  history
        tableWidth      = widthTable rowWidthPointer rowWidthStack rowWidthHeap rowWidthGlobal
     in hline tableWidth ++ "\n" ++
         rowNaming rowWidthPointer rowWidthStack rowWidthHeap rowWidthGlobal ++ "\n" ++
         concatMap (showSnapshot rowWidthPointer rowWidthStack rowWidthHeap rowWidthGlobal
                    tableWidth nameLengthStack nameLengthHeap nameLengthCode descriptions)
                   (reverse history) ++ hline tableWidth

-- show current state including last executed instruction
-- rowStack includes visualization of top and helpful descriptions for code cells
showSnapshot :: RowWidth -> RowWidth -> RowWidth -> RowWidth -> TableWidth -> NameLength ->
                NameLength -> NameLength -> [CodeCellDescription] -> Snapshot -> String
showSnapshot rowWidthPointer rowWidthStack rowWidthHeap rowWidthGlobal tableWidth nameLengthStack
    nameLengthHeap nameLengthCode descriptions
    (Snapshot (State (pc, _, (stack, top), heap, global), instruction)) =
      let rowPointer      = pointerRow  rowWidthPointer instruction top pc
          rowStack        = stackRow    rowWidthStack   nameLengthStack nameLengthHeap nameLengthCode
                                        top  descriptions stack
          rowHeap         = heapRow     rowWidthHeap    nameLengthHeap  heap
          rowGlobal       = globalRow   rowWidthGlobal  global          heap
          -- identify required number of lines
          requiredLengt   =
              maximum [length rowPointer, length rowStack, length rowHeap, length rowGlobal]
          -- append rows with correct padding
          rowList       = map (padRow requiredLengt)
                              [(rowPointer, rowWidthPointer), (rowStack,rowWidthStack),
                               (rowHeap,rowWidthHeap), (rowGlobal,rowWidthGlobal)]
       in case rowList of
           (r1:r2:r3:r4:_) -> hline tableWidth ++ "\n" ++
                              concat (zipWith4 (\s1 s2 s3 s4 -> "| "++s1++s2++s3++s4++" |\n")
                                                        r1 r2 r3 r4)
           _                  -> "Abstract Machine error: constructing output failed"

-- return padded list of instruction, top and program counter
pointerRow :: RowWidth -> Instruction -> Top -> PC -> [String]
pointerRow rowWidth instruction top pc = map (padCell rowWidth)
                                             [showI instruction, showT top, showP pc]

-- return padded list of stack cells in more readable format prepended with address
-- includes visualization of top and helpful descriptions for Code addresses
stackRow :: RowWidth -> NameLength -> NameLength -> NameLength -> Top -> [CodeCellDescription] ->
            [StackElement] -> [String]
stackRow rowWidht sNameLength hNameLength cNameLength top descriptions stack =
    let lenWoDesc = max hNameLength cNameLength
        rawRow    =
            -- +2: take ": " into account
            zipWith (++) [padName (sNameLength + 2) ("s" ++ show x ++ ": ") | x <- [0..] :: [Int]]
                         (map (showS descriptions lenWoDesc) stack)
     in map (padCell rowWidht) $ addTopPointer top rawRow

-- append visualization of Top at specified position
addTopPointer :: Top -> [String] -> [String]
addTopPointer _   []       = []
addTopPointer top rowStack
  | top < 0 = rowStack
addTopPointer top rowStack = take top rowStack ++
                             (rowStack !! top ++ " <-- Top") : drop (top + 1) rowStack

-- return padded list of heap cells prepended with address
heapRow :: RowWidth -> NameLength -> Heap -> [String]
heapRow rowWidht nameLength heap =
    -- +2: take ": " into account
    map (padCell rowWidht) $ zipWith (++) [padName (nameLength + 2) ("h" ++ show x ++ ": ")
                                            | x <- [0..] :: [Int]]
                                          (map show heap)

-- return padded list pointers stored in global at correct position with respect to heapRow
globalRow :: RowWidth -> Global -> Heap -> [String]
globalRow rowWidht global heap =
    _gR global heap [] where
        _gR glob h rowGlobal
          | length rowGlobal == heapLength = rowGlobal
          | otherwise = case lookup (length rowGlobal) swappedGlobal of
                          Just fun  -> _gR glob h (rowGlobal ++ [padCell rowWidht
                                                                    ("<-- \"" ++ fun ++ "\"")])
                          Nothing   -> _gR glob h (rowGlobal ++ [padCell rowWidht ""])
              where heapLength = length heap
                    swappedGlobal = map swap global

-- calculate total width for frame
widthTable :: RowWidth -> RowWidth -> RowWidth -> RowWidth -> TableWidth
widthTable rowWidthPointer rowWidthStack rowWidthHeap rowWidthGlobal =
    4 + sum (map (max defaultRowWidth) [rowWidthPointer,rowWidthStack,rowWidthHeap,rowWidthGlobal])

-- calculate minimal row width for pointer row
-- +4: take leading "I: " etc. into account and guarantee one space distance between rows
maxRowWidthPointer :: History -> RowWidth
maxRowWidthPointer (Snapshot (State (_, code, _, _, _), _):_) = 4 + foldr (max.length.show) 0 code
maxRowWidthPointer _                                          = defaultRowWidth

-- calculate minimal row width for stack row
-- +14: take Top pointer, ": " after names and () for descriptions into account and guarantee one space
-- distance between rows
maxRowWidthStack :: History -> [CodeCellDescription] -> RowWidth
maxRowWidthStack history descriptions =
    14 + maxNameLengthStack history + max (maxNameLengthHeap history) (maxNameLengthCode history) +
        foldr (max.length.snd) 0 descriptions

-- calculate minimal row width for heap row
-- +1: guarantee one space distance between rows
maxRowWidthHeap :: History -> RowWidth
maxRowWidthHeap history = 1 +  maxHeapCellLength history

-- calculate minimal row width for global row
-- +6: take arrows and "" surrounding function names into account
maxRowWidthGlobal :: History -> RowWidth
maxRowWidthGlobal history = 6 + foldr (max.maxLengthGlobalContent) 0 history

-- get maximal heap cell length with padding in mind
-- +2: take ": " after heap cell names into account
maxHeapCellLength :: History -> NameLength
maxHeapCellLength history = 2 + maxNameLengthHeap history + foldr (max.maxLengthHeapContent) 0 history

-- get maximal function name length
maxLengthGlobalContent :: Snapshot -> NameLength
maxLengthGlobalContent (Snapshot (State (_, _, _, _, global), _)) = foldr (max.length.fst) 0 global

-- get maximal length of heap cells in snapshot
maxLengthHeapContent :: Snapshot -> NameLength
maxLengthHeapContent (Snapshot (State (_, _, _, heap, _), _)) = foldr (max.length.show) 0 heap

-- get maximal length of stack cell names which are of form 's0'
maxNameLengthStack :: History -> NameLength
maxNameLengthStack history = 1 + length (show (foldr (max.stackLongestAddress) 0 history))

-- get length of stack for current snapshot
stackLongestAddress :: Snapshot -> RowLength
stackLongestAddress (Snapshot (State (_, _, (stack, _), _, _), _)) = length stack - 1

-- get maximal length of heap cell names which are of form 'h0'
maxNameLengthHeap :: History -> NameLength
maxNameLengthHeap history = 1 + length (show (foldr (max.heapLongestAddress) 0 history))

-- get length of heap for current snapshot
heapLongestAddress :: Snapshot -> RowLength
heapLongestAddress (Snapshot (State (_, _, _, heap, _), _)) = length heap - 1

-- get maximal length of code cell names which are of form 'c0'
maxNameLengthCode :: History -> NameLength
maxNameLengthCode history = let Snapshot (State (_, code, _, _, _), _) = head history in
                                1 + length (show (length code))

-- show instruction
showI :: Instruction -> String
showI instruction = "I: " ++ show instruction

-- show top
showT :: Top -> String
showT top = "T: s" ++ show top

-- show program counter PC
showP :: PC -> String
showP pc = "P: c" ++ show pc

-- more readable version of stack elements in the context of showSnapshot
-- includes useful descriptions for code cells
showS :: [CodeCellDescription] -> NameLength -> StackElement -> String
showS descriptions nameLength (CODE codeAddress) =
    padName nameLength ("c" ++ show codeAddress) ++
    case lookup codeAddress descriptions of
      Just description -> " (" ++ description ++ ")"
      _                -> ""
showS _            nameLength (HEAP heapAddress) = padName nameLength ("h" ++ show heapAddress)

-- pad name with space until name length is reached
padName :: RowWidth -> String -> String
padName len string = string ++ replicate (len - length string) ' '

-- pad cell with space until required row width is reached which is either specified or defaultRowWidth
padCell :: RowWidth -> String -> String
padCell len string = string ++ replicate (max defaultRowWidth len - length string) ' '

-- append correctly padded cells to current row
padRow :: RowLength -> ([String],RowWidth) -> [String]
padRow requiredLengt (row, rowWidht) =
    let rowLength = length row in
        if rowLength < requiredLengt
           then row ++ replicate (requiredLengt - rowLength)
                                 (replicate (max rowWidht defaultRowWidth) ' ')
           else row

-- first line of the table showing snapshot history introducing row naming
rowNaming :: RowWidth -> RowWidth -> RowWidth -> RowWidth -> String
rowNaming rowWidthPointer rowWidthStack rowWidthHeap rowWidthGlobal =
    "| " ++ padCell rowWidthPointer "Pointer" ++ padCell rowWidthStack "Stack" ++
    padCell rowWidthHeap "Heap" ++ padCell rowWidthGlobal "Global" ++ " |"

-- generate top and bottom border for the frame
hline :: TableWidth -> String
hline width = "+" ++ replicate (width-2) '-' ++ "+"

{- Helper functions for executing the abstract machine and treating the execution's result
      saveSnapshot
      codeCellDescriptions
      functionDefinitionBeginnings
      returnAddresses
      showMachineSteps
      showResult
    -}

-- save snapshot of state to the history
saveSnapshot :: RuntimeEnv -> Snapshot -> RuntimeEnv
saveSnapshot (history, trace, codeDesc) snapshot = (snapshot : history, trace, codeDesc)

-- create code cell descriptions from initial machine state
codeCellDescriptions :: Heap -> Code -> [CodeCellDescription]
codeCellDescriptions heap code =
    let functionDefBegs = functionDefinitionBeginnings heap
     in functionDefBegs ++ returnAddresses code functionDefBegs

-- function to initialize descriptions for code addresses from heap
functionDefinitionBeginnings :: Heap -> [CodeCellDescription]
functionDefinitionBeginnings heap = _fDB heap [] where
    _fDB []                  descriptions = descriptions
    _fDB (DEF name _ adr:xs) descriptions = _fDB xs $ (adr, name):descriptions
    _fDB (_             :xs) descriptions = _fDB xs descriptions

-- add annotations for return addresses to the code cell descriptions
returnAddresses :: Code -> [CodeCellDescription] -> [CodeCellDescription]
returnAddresses c fDB = _rA c fDB [] where
    _rA _    []     rAdrDesc = map (\(a, d) -> (a, "return to \"" ++ d ++ "\"")) rAdrDesc
    _rA code (d@(adr, name):ds) rAdrDesc =
        let Just returnAdr = (+) <$> (elemIndex Call . drop adr) code <*> pure adr
         in case lookup adr rAdrDesc of
              Nothing -> _rA code ds $ (returnAdr, name):rAdrDesc
              _       -> _rA code ds $ d:rAdrDesc

-- show number of instructions executed by the abstract machine
showMachineSteps :: (String, RuntimeEnv) -> String
showMachineSteps (_, (history, _, _)) = show . length $ history

-- show result of program execution
showResult :: (String, RuntimeEnv) -> String
showResult = fst


{- Main functions to execute the abstract machine
    execute
    mainLoop
   -}

-- initialize abstract machine and generate output
execute :: Either String State -> (String, RuntimeEnv)
execute compiledState = case mainLoop (compiledState, ([],[],[])) of
        (Left s, (history, trace, descriptions)) ->
            if "Runtime error:" `isPrefixOf` s
               then (s ++ "\nTraceback most recent call first: " ++ intercalate ", " trace,
                     (history, trace, descriptions))
               else (s, (history, trace, descriptions))
        (Right (State (_, _, (stack, _), heap, _)), env) -> (result, env)
            where HEAP hAdr = head stack
                  result = case heap !! hAdr of
                        VAL hcell -> do
                            case hcell of
                                Truthval bool -> "Result: " ++ show bool
                                Integer int   -> "Result: " ++ show int
                        IND hcell -> do
                            case value hcell heap of
                                VAL (Truthval bool) -> "Result: " ++ show bool
                                VAL (Integer int)   -> "Result: " ++ show int
                                _ -> "Abstract Machine error: Result is no Value Cell"
                        _ -> "Abstract Machine error: Result is no Value Cell"

-- execute machine instruction on machine state while saving useful information to
-- runtime environment
mainLoop :: (Either String State, RuntimeEnv) -> (Either String State, RuntimeEnv)
{- deprecated, used for debugging
    mainLoop (Right (State (pc, code, stack, heap, global)))
  | traceShow (Snapshot (State (pc, code, stack, heap, global), code !! pc)) False = undefined -}
mainLoop (state, (history, trace, descriptions)) = case state of
    Left errorMsg -> (Left errorMsg, (history, trace, descriptions))
    Right (State(pc, code, (stack, top), heap, global)) -> case instruction of
        Halt -> result
        _    -> mainLoop result
        where instruction = code !! pc
              newState    = fromRight (State (pc, code, (stack, top), heap, global)) nextStep
              result = case instruction of
                -- initialize code cell descriptions
                Reset -> (nextStep, saveSnapshot (history, trace, codeCellDescriptions heap code)
                            $ Snapshot (newState, instruction))
                -- save description for return address and add called function to trace
                Call  -> let HEAP topStackHadr = stack !! top in
                             case value topStackHadr heap of
                               DEF fName _ _ ->
                                      (nextStep, saveSnapshot (history, fName:trace,
                                            descriptions)
                                            $ Snapshot (newState, instruction))
                               _ -> (nextStep, saveSnapshot (history, trace, descriptions)
                                     $ Snapshot (newState, instruction))
                _     -> (nextStep, saveSnapshot (history, trace, descriptions)
                            $ Snapshot (newState, instruction))
              nextStep = case instruction of
                Halt -> halt state
                Reset -> do
                    State (newPc, _, (newStack, newTop), newHeap, newGlobal) <- reset state
                    Prelude.return $ State (newPc + 1, code, (newStack, newTop), newHeap, newGlobal)
                Pushfun f -> do
                    State (_, _, (newStack, newTop), newHeap, newGlobal) <- pushFun f state
                    Prelude.return $ State (pc + 1, code, (newStack, newTop), newHeap, newGlobal)
                Pushval v -> do
                    State (_, _, (newStack, newTop), newHeap, newGlobal) <- pushVal (VAL v) state
                    Prelude.return $ State (pc + 1, code, (newStack, newTop), newHeap, newGlobal)
                Pushparam p -> do
                    State (_, _, (newStack, newTop), newHeap, newGlobal) <- pushParam p state
                    Prelude.return $ State (pc + 1, code, (newStack, newTop), newHeap, newGlobal)
                Makeapp -> do
                    State (_, _, (newStack, newTop), newHeap, newGlobal) <- makeApp state
                    Prelude.return $ State (pc + 1, code, (newStack, newTop), newHeap, newGlobal)
                Slide a -> do
                    State (_, _, (newStack, newTop), newHeap, newGlobal) <- slide a state
                    Prelude.return $ State (pc + 1, code, (newStack, newTop), newHeap, newGlobal)
                Return -> do
                    State (newPc, _, (newStack, newTop), newHeap, newGlobal) <- AbstractMachine.return state
                    Prelude.return $ State (newPc + 1, code, (newStack, newTop), newHeap, newGlobal)
                Unwind -> do
                    State (newPc, _, (newStack, newTop), newHeap, newGlobal) <- unwind state
                    Prelude.return $ State (newPc + 1, code, (newStack, newTop), newHeap, newGlobal)
                Call -> do
                    State (newPc, _, (newStack, newTop), newHeap, newGlobal) <- call state
                    Prelude.return $ State (newPc, code, (newStack, newTop), newHeap, newGlobal)
                Pushpre co -> do
                    State (_, _, (newStack, newTop), newHeap, newGlobal) <- pushPre co state
                    Prelude.return $ State (pc + 1, code, (newStack, newTop), newHeap, newGlobal)
                Update ok -> do
                    State (_, _, (newStack, newTop), newHeap, newGlobal) <- update ok state
                    Prelude.return $ State (pc + 1, code, (newStack, newTop), newHeap, newGlobal)
                Operator ok -> do
                    State (_, _, (newStack, newTop), newHeap, newGlobal) <- operator ok state
                    Prelude.return $ State (pc + 1, code, (newStack, newTop), newHeap, newGlobal)
                Alloc -> do
                    State (_, _, (newStack, newTop), newHeap, newGlobal) <- alloc state
                    Prelude.return $ State (pc + 1, code, (newStack, newTop), newHeap, newGlobal)
                UpdateLet n -> do
                    State (_, _, (newStack, newTop), newHeap, newGlobal) <- updateLet n state
                    Prelude.return $ State (pc + 1, code, (newStack, newTop), newHeap, newGlobal)
                SlideLet n -> do
                    State (_, _, (newStack, newTop), newHeap, newGlobal) <- slideLet n state
                    Prelude.return $ State (pc + 1, code, (newStack, newTop), newHeap, newGlobal)
