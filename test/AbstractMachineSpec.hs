module AbstractMachineSpec (spec) where

import Test.Hspec

import Compiler
    ( CalcOp(OpTimes, OpSubUn, OpSubBi, OpDiv, OpAnd, OpOr, OpEq,
             OpLess, OpIf, OpAdd, OpNot),
      HeapCell(UNINITIALIZED, PRE, APP, VAL, DEF, IND),
      Instruction(Alloc, Operator, Halt, Return, Pushpre, Update, Slide,
                  Unwind, Call, Makeapp, Pushparam, Reset, Pushfun, Pushval, UpdateLet, SlideLet),
      OperatorKind(OpInt, If, Op),
      PossibleTypes(Integer, Truthval),
      StackElement(HEAP, CODE),
      State(State) )
import AbstractMachine
    ( add2arg,
      address,
      alloc,
      arity,
      call,
      halt,
      makeApp,
      new,
      operator,
      pushFun,
      pushParam,
      pushPre,
      pushVal,
      reset,
      return,
      slide,
      slideLet,
      unwind,
      update,
      updateLet,
      value )

spec :: Spec
spec = do
    describe "address" $ do
        it "returns Left if function name is not found" $ do
            address "f" [] `shouldBe` Left "Compilation error: Function \"f\" not defined."
        it "returns HeapAddress" $ do
            address "f" [("f",1)] `shouldBe` Right 1

    describe "add2arg" $ do
        it "returns second value of an APP cell" $ do
            add2arg 0 [APP 2 3] `shouldBe` Right 3
        it "calls recursively if arriving at IND cell" $ do
            add2arg 0 [IND 1, APP 2 3] `shouldBe` Right 3
        it "returns Left for any other cell" $ do
            add2arg 0 [DEF "f" 0 0]
                `shouldBe` Left "Compilation error: HeapCell \"DEF \"f\" 0 0\" referenced by \"0\" is no APP-Cell."
            add2arg 0 [VAL (Truthval True)]
                `shouldBe` Left "Compilation error: HeapCell \"VAL (Truthval True)\" referenced by \"0\" is no APP-Cell."
            add2arg 0 [PRE OpAdd 2]
                `shouldBe` Left "Compilation error: HeapCell \"PRE OpAdd 2\" referenced by \"0\" is no APP-Cell."

    describe "new" $ do
        it "adds heap cell and returns the added cell's address and the new heap" $ do
            new (IND 1) []      `shouldBe` (0, [IND 1])
            new (IND 2) [IND 1] `shouldBe` (1, [IND 1, IND 2])

    -- redundant since function is deprecated
    -- describe "typ" $ do
    --     it "returns the type of the value stored in a VAL cell" $ do
    --         typ 0 [VAL (Truthval True)] `shouldBe` Right (Truthval True)
    --         typ 0 [VAL (Integer 30)]    `shouldBe` Right (Integer 1)
    --     it "returns Left if referenced HeapCell isn't a VAL cell" $ do
    --         typ 0 [APP 2 3]    
    --             `shouldBe` Left ("Compilation error: " ++
    --                             "HeapCell \"APP 2 3\" referenced by \"0\" is no VAL-Cell.")
    --         typ 0 [DEF "f" 0 0]
    --             `shouldBe` Left ("Compilation error: " ++
    --                             "HeapCell \"DEF \"f\" 0 0\" referenced by \"0\" is no VAL-Cell.")
    --         typ 0 [IND 1]      
    --             `shouldBe` Left ("Compilation error: " ++
    --                             "HeapCell \"IND 1\" referenced by \"0\" is no VAL-Cell.")
    --         typ 0 [PRE OpAdd 2]
    --             `shouldBe` Left ("Compilation error: " ++
    --                             "HeapCell \"PRE OpAdd 2\" referenced by \"0\" is no VAL-Cell.")

    describe "value" $ do
        it "returns HeapCell referenced by IND cell at HeapAddress" $ do
            value 1 [IND 2, IND 0, APP 2 3] `shouldBe` APP 2 3
  --    it "returns HeapCell referenced by IND cell at HeapAddress" $ do
  --        value 1 [IND 0, IND 0] `shouldBe` IND 0
        it "returns HeapCell if HeapAddress points to non-IND-cell" $ do
            value 0 [VAL (Truthval True)] `shouldBe` VAL (Truthval True)
            value 0 [APP 2 3]             `shouldBe` APP 2 3
            value 0 [DEF "f" 0 0]         `shouldBe` DEF "f" 0 0
            value 0 [PRE OpAdd 2]         `shouldBe` PRE OpAdd 2

    describe "arity" $ do
        it "returns the arity of a predefined operator" $ do
            arity OpNot   `shouldBe` 1
            arity OpSubUn `shouldBe` 1
            arity OpIf    `shouldBe` 3
            arity OpAdd   `shouldBe` 2
            arity OpSubBi `shouldBe` 2
            arity OpTimes `shouldBe` 2
            arity OpDiv   `shouldBe` 2
            arity OpAnd   `shouldBe` 2
            arity OpOr    `shouldBe` 2
            arity OpEq    `shouldBe` 2
            arity OpLess  `shouldBe` 2

    describe "operator" $ do
        it "treats unary operators correctly" $ do
            let rAdr = 10
                rightState    = Right $ State (0, [Operator (OpInt 1)],
                                                ([HEAP 2, HEAP 1, CODE rAdr, HEAP 0], 3),
                                                [VAL (Truthval True), PRE OpNot 1, APP 2 1],
                                                [("undefined", 0)])
                newRightState = Right $ State (0, [Operator (OpInt 1)], ([HEAP 2, CODE rAdr, HEAP 3], 2),
                                                [VAL (Truthval True), PRE OpNot 1, APP 2 1,
                                                 VAL (Truthval False)],
                                                [("undefined", 0)])
            operator (OpInt 1) rightState `shouldBe` newRightState
        it "treats binary operators correctly" $ do
            let rAdr = 10
                rightState    = Right $ State (0, [Operator (OpInt 2)],
                                               ([HEAP 4, HEAP 3, HEAP 2, CODE rAdr, HEAP 1, HEAP 0], 5),
                                               [VAL (Integer 0), VAL (Integer 1), PRE OpTimes 2, APP 2 1,
                                                APP 3 0],
                                               [("undefined", 0)])
                newRightState = Right $ State (0, [Operator (OpInt 2)], ([HEAP 4, CODE rAdr, HEAP 5], 2),
                                    [VAL (Integer 0), VAL (Integer 1), PRE OpTimes 2, APP 2 1, APP 3 0,
                                     VAL (Integer 0)], [("undefined", 0)])
            operator (OpInt 2) rightState `shouldBe` newRightState
        it "works if the if condition is a truth value" $ do
            let rAdr     = 10
                _ifTrue  = True
                _ifFalse = False
                _then    = True
                _else    = False
                heap cond           = [VAL (Truthval _else), VAL (Truthval _then), VAL (Truthval cond),
                                        PRE OpIf 3, APP 3 2, APP 4 1, APP 5 0]
                rightState cond     = Right $ State (0, [Operator If],
                                                 ([HEAP 6, HEAP 5, HEAP 4, HEAP 3, CODE rAdr, HEAP 2], 5),
                                                 heap cond, [("undefined", 0)])
                newRightState cond  = Right $ State (0, [Operator If], ([HEAP 6, CODE rAdr,
                                                 HEAP (if cond then 1 else 0)], 2),
                                                 heap cond, [("undefined", 0)])
            operator If (rightState _ifTrue)  `shouldBe` newRightState _ifTrue
            operator If (rightState _ifFalse) `shouldBe` newRightState _ifFalse
        it "doesn't work if the if condition is a number" $ do
            let rAdr       = 10
                heap       = [VAL (Truthval False), VAL (Truthval True), VAL (Integer 1),
                              PRE OpIf 3, APP 3 2, APP 4 1, APP 5 0]
                rightState = Right $ State (0, [Operator If],
                                            ([HEAP 6, HEAP 5, HEAP 4, HEAP 3, CODE rAdr, HEAP 2], 5),
                                            heap, [("undefined", 0)])
            operator If rightState `shouldBe` Left "Runtime error: if condition not boolean"
        it "passes through error messages thrown by add2arg when called on if" $ do
            let rAdr       = 10
                heap       = [VAL (Truthval False), VAL (Truthval True), VAL (Truthval True), PRE OpIf 3,
                              APP 3 2, VAL (Truthval True), APP 5 0]
                rightState = Right $ State (0, [Operator If],
                                            ([HEAP 6, HEAP 5, HEAP 4, HEAP 3, CODE rAdr, HEAP 2], 5),
                                            heap, [("undefined", 0)])
            operator If rightState `shouldBe`
                Left "Compilation error: HeapCell \"VAL (Truthval True)\" referenced by \"5\" is no APP-Cell."

    describe "halt" $ do
        it "returns the current state if current stack contains only one element" $ do
            let rightState = Right $ State (0, [Halt], ([HEAP 0], 0), [VAL (Truthval True)], [("undefined", 0)])
            halt rightState `shouldBe` rightState
        it "returns Left if stack contains more than one element, hence program wasn't executed correctly" $ do
            let rightState = Right $ State (0, [Halt], ([HEAP 0, CODE 0], 1),
                                            [VAL (Integer 1)], [("undefined", 0)])
            halt rightState `shouldBe` Left ("Compilation error: Stack holds more than one element, " ++
                                             "execution aborted.\n Stack content:\n[HEAP 0,CODE 0]")
        it "returns Left if stack is empty" $ do
            let rightState = Right $ State (0, [Halt], ([], 0),
                                            [VAL (Integer 1)], [("undefined", 0)])
            halt rightState `shouldBe` Left "Compilation error: Stack is empty, no result available"

    describe "return" $ do
        it "returns to the code address saved as return address and preserves function's result" $ do
            let rightState    = Right $ State (1, [Halt,Return], ([HEAP 9, HEAP 9, CODE 0, HEAP 0], 3),
                                                [VAL (Integer 1)], [("undefined", 0)])
            let newRightState = Right $ State (0, [Halt,Return], ([HEAP 9, HEAP 9, HEAP 0], 2),
                                                [VAL (Integer 1)], [("undefined", 0)])
            AbstractMachine.return rightState `shouldBe` newRightState

    describe "pushPre" $ do
        it "saves predefined operation to heap and pushes its pointer to the stack" $ do
            let rightState    = Right $ State (0, [Pushpre OpAdd], ([CODE 0], 0),
                                                [], [("undefined", 0)])
            let newRightState = Right $ State (0, [Pushpre OpAdd], ([CODE 0, HEAP 0], 1),
                                                [PRE OpAdd 2], [("undefined", 0)])
            pushPre OpAdd rightState `shouldBe` newRightState

    describe "update" $ do
        it "treats predefined operators right" $ do
            let rightState    = Right $ State (0, [Update Op], ([HEAP 2, CODE 0, HEAP 3], 2),
                                               [VAL (Truthval True), PRE OpNot 1, APP 2 1,
                                                VAL (Truthval False)], [("undefined", 0)])
                newRightState = Right $ State (0, [Update Op], ([CODE 0, HEAP 2], 1),
                                               [VAL (Truthval True), PRE OpNot 1, VAL (Truthval False),
                                                VAL (Truthval False)], [("undefined", 0)])
            update Op rightState `shouldBe` newRightState
        it "treats function defined in program right" $ do
            -- no correct compiler output but minimal working example
            let rightState    = Right $ State (0, [Update (OpInt 1)], ([HEAP 2, HEAP 0, CODE 0, HEAP 4], 3),
                                               [DEF "g" 1 0, VAL (Truthval True), APP 0 1, PRE OpNot 1,
                                                APP 3 1], [("g", 0)])
                newRightState = Right $ State (0, [Update (OpInt 1)], ([HEAP 2, HEAP 0, CODE 0, HEAP 4], 3),
                                               [DEF "g" 1 0, VAL (Truthval True), IND 4, PRE OpNot 1,
                                                APP 3 1], [("g", 0)])
            update (OpInt 1) rightState `shouldBe` newRightState
    
    describe "slide" $ do 
        it "clears the stack by the specified amount of cells under the two topmost cells" $ do
            let rightState    = Right $ State (0, [Slide 2], ([CODE 3, HEAP 8, HEAP 1, CODE 40, HEAP 11],4),
                                               [],[("undefined", 0)])
                newRightState = Right $ State (0, [Slide 2], ([CODE 3, CODE 40, HEAP 11],2),
                                               [],[("undefined", 0)])
            slide 2 rightState `shouldBe` newRightState

    describe "unwind" $ do -- Note: incrementing is done in the main loop
        it "places the address of the first parameter of an APP cell referenced by the top stack cell and deincrements the pc" $ do
            let rightState    = Right $ State (0, [Unwind], ([CODE 3, HEAP 8],1),[IND 8, DEF "quadrat" 1 41,
                                               VAL (Integer 1), VAL (Integer 3), PRE OpTimes 2, APP 4 3, APP 5 2, APP 1 6,
                                                APP 1 7],[("undefined", 0)])
               
                newRightState = Right $ State (-1, [Unwind], ([CODE 3, HEAP 8, HEAP 1],2),[IND 8, 
                                               DEF "quadrat" 1 41, VAL (Integer 1), VAL (Integer 3), 
                                                PRE OpTimes 2, APP 4 3, APP 5 2, APP 1 6, APP 1 7],[("undefined", 0)])
            
            unwind rightState `shouldBe` newRightState
        it "does nothing if the cell referenced by the top stack cell is not an APP cell" $ do
            let rightState    = Right $ State (0, [Unwind], ([CODE 3, HEAP 8],1),[IND 8, DEF "quadrat" 1 41, 
                                               VAL (Integer 1), VAL (Integer 3), PRE OpTimes 2,
                                                APP 4 3, APP 5 2, APP 1 6, VAL (Integer 1)],[("undefined", 0)])
               
                newRightState = Right $ State (0, [Unwind], ([CODE 3, HEAP 8],1),[IND 8, DEF "quadrat" 1 41,
                                                VAL (Integer 1), VAL (Integer 3), PRE OpTimes 2,
                                                 APP 4 3, APP 5 2, APP 1 6, VAL (Integer 1)],[("undefined", 0)])
            
            unwind rightState `shouldBe` newRightState

    describe "call" $ do
        it "sets the pc to the location of the user defined function referenced by the top stack cell and saves the current pc to the top of the stack" $ do
            let rightState    = Right $ State (2, [Reset, Pushfun "main", Call], ([HEAP 0], 0),
                                               [DEF "main" 0 27, DEF "quadrat" 1 41],[("undefined", 0)])
                
                newRightState = Right $ State (27, [Reset, Pushfun "main", Call], ([HEAP 0, CODE 2], 1),
                                               [DEF "main" 0 27, DEF "quadrat" 1 41],[("undefined", 0)])

            call rightState `shouldBe` newRightState

    describe "makeApp" $ do
        it "consolidates the top two stack cells into an APP cell" $ do
            let rightState    = Right $ State (0, [Makeapp], ([HEAP 0, CODE 3, HEAP 2, HEAP 3, HEAP 4], 4), 
                                               [DEF "main" 0 27, DEF "quadrat" 1 41, VAL (Integer 1), VAL (Integer 3), 
                                                PRE OpTimes 2], [("undefined", 0)])
                        
                newRightState = Right $ State (0, [Makeapp], ([HEAP 0, CODE 3, HEAP 2, HEAP 5], 3), 
                                               [DEF "main" 0 27, DEF "quadrat" 1 41, VAL (Integer 1), VAL (Integer 3), 
                                                PRE OpTimes 2, APP 4 3], [("undefined", 0)])

            makeApp rightState `shouldBe` newRightState

    describe "pushParam" $ do
        it "loads the address of the first argument of a function" $ do
            let rightState    = Right $ State (0, [Pushparam 1], ([CODE 3, HEAP 8, HEAP 1, CODE 40], 3), 
                                               [IND 8, DEF "quadrat" 1 41, VAL (Integer 1), VAL (Integer 3), 
                                                PRE OpTimes 2, APP 4 3, APP 5 2, APP 1 6, APP 1 7], [("undefined", 0)])
                        
                newRightState = Right $ State (0, [Pushparam 1], ([CODE 3, HEAP 8, HEAP 1, CODE 40, HEAP 7], 4), 
                                               [IND 8, DEF "quadrat" 1 41, VAL (Integer 1), VAL (Integer 3), 
                                                PRE OpTimes 2, APP 4 3, APP 5 2, APP 1 6, APP 1 7], [("undefined", 0)])

            pushParam 1 rightState `shouldBe` newRightState

    describe "reset" $ do
        it "sets the pc to 0 and the top to -1" $ do
            let rightState    = Right $ State (0, [Reset], ([], 4), [DEF "main" 0 27, DEF "quadrat" 1 41], [("undefined", 0)])

                newRightState = Right $ State (0, [Reset], ([], -1), [DEF"main" 0 27, DEF "quadrat" 1 41], [("undefined", 0)])

            reset rightState `shouldBe` newRightState

    describe "pushFun" $ do
        it "loads a user-defined function onto the stack" $ do
            let rightState    = Right $ State (1, [Reset, Pushfun "main"], ([], -1), 
                                               [DEF "main" 0 27, DEF "quadrat" 1 41], [("main", 0)])
                        
                newRightState = Right $ State (1, [Reset, Pushfun "main"], ([HEAP 0], 0), 
                                               [DEF "main" 0 27, DEF "quadrat" 1 41], [("main", 0)])

            pushFun "main" rightState `shouldBe` newRightState

    describe "pushVal" $ do
        it "loads the address of a value onto the stack" $ do
            let rightState    = Right $ State (0, [Pushval (Integer 9)], ([HEAP 0, CODE 3, HEAP 3, HEAP 6], 3), 
                                               [DEF "main" 0 27, IND 4, UNINITIALIZED, APP 2 1, UNINITIALIZED, UNINITIALIZED, APP 5 4], [("main", 0)])
                        
                newRightState = Right $ State (0, [Pushval (Integer 9)], ([HEAP 0, CODE 3, HEAP 3, HEAP 6, HEAP 7], 4), 
                                               [DEF "main" 0 27, IND 4, UNINITIALIZED, APP 2 1, UNINITIALIZED, UNINITIALIZED, APP 5 4, VAL (Integer 9)], [("main", 0)])

            pushVal (VAL (Integer 9)) rightState `shouldBe` newRightState

    describe "alloc" $ do
        it "creates a new memory(healCell) at the top of the stack" $ do
            let rightState    = Right $ State (0, [Alloc], ([HEAP 0, CODE 3], 0), 
                                               [DEF "main" 0 27], [("undefined", 0)])
                                        
                newRightState = Right $ State (0, [Alloc], ([HEAP 0, CODE 3, HEAP 1], 1), 
                                                [DEF "main" 0 27, UNINITIALIZED], [("undefined", 0)])
                
            alloc rightState `shouldBe` newRightState
                
    describe "updateLet" $ do
        it "updates the heapCell, the result of add2arg (stack !! (top - n -1) heap), in the heap as (IND top) and eliminates the top element in the stack" $ do
            let rightState    = Right $ State (0, [UpdateLet 0], ([HEAP 0, CODE 3, HEAP 3, HEAP 6, HEAP 7], 4), 
                                                [DEF "main" 0 27, IND 4, UNINITIALIZED, APP 2 1, UNINITIALIZED, UNINITIALIZED, APP 5 4, VAL (Integer 9)], [("main", 0)])
                                        
                newRightState = Right $ State (0, [UpdateLet 0], ([HEAP 0, CODE 3, HEAP 3, HEAP 6], 3), 
                                                [DEF "main" 0 27, IND 4, UNINITIALIZED, APP 2 1, IND 7, UNINITIALIZED, APP 5 4, VAL (Integer 9)], [("main", 0)])
                
            updateLet 0 rightState `shouldBe` newRightState
                        
    describe "slideLet" $ do
        it "eliminates the n stack elements below the top element in the stack" $ do
            let rightState    = Right $ State (0, [SlideLet 2], ([HEAP 0, CODE 3, HEAP 3, HEAP 6, HEAP 1], 4), 
                                                [DEF "main" 0 27, IND 4, UNINITIALIZED, APP 2 1, IND 7, UNINITIALIZED, APP 5 4, VAL (Integer 9)], [("main", 0)])
                                                
                newRightState = Right $ State (0, [SlideLet 2], ([HEAP 0, CODE 3, HEAP 1], 2), 
                                                 [DEF "main" 0 27, IND 4, UNINITIALIZED, APP 2 1, IND 7, UNINITIALIZED, APP 5 4, VAL (Integer 9)], [("main", 0)])
                        
            slideLet 2 rightState `shouldBe` newRightState
